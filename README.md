1.How to verify the program correctness?

The program implemented the naive solution for correlated-corelation and the improved version using the idea of im2col.

A verification util is implemented to compare the results for all possible valid combination 
of 

{(n,k) | n in [7,64], k in [1,11], k%2=1, (n,k) in NxN} 

Eigen3 is used for matrix multiplication

To build and run the program, just following the usual way of building CMake project :

```asm
mkdir build
cd build
cmake ..
./lightmatter
```

It is tested with the following environment:

```
cpchung:im2col$ uname -a
Linux cpchung 4.15.0-88-generic #88-Ubuntu SMP Tue Feb 11 20:11:34 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
cpchung:im2col$ g++ --version
g++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

```


2.Static memory allocation  using raw array does not seem to work very well with Eigen3. 
So I use vector<vector<int>> to model matrix, 
though the input requirement is small enough to use static allocation on stack.

3.Brief description of the implementation

For the direct discrete cross-correlation:

I am assuming input is only for integer 
hence the complex conjugate for the filter function is ignored and the implementation is straightforward.

For the im2col convolution: 

The geometric interpretation of the direct 2D discrete convolution operation 
resemmbles sliding window with the filter mask over the input matrix. Specifically, each
entry of the result matrix is the sum over the component-wise multiplication 
btw the filter and corresponding block of the input matrix. In order to formulate this as a GEMM problem, 
the k^2 filter matrix can be flattened to (1,k^2) row vector 
and each k^2 block of the input matrix can be flattened to a (k^2,1) column vector.
Then we can concatenate all the n^2 flattened (k^2,1) column vector into (k^2,n^2) matrix 
and then perform GEMM with the (1,k^2) row vector from the filter. So the dimension of the resulting matrix is
(1,k^2) * (k^2,n^2) = (1, n^2). The desired result is from reshaping this (1, n^2) matrix back to (n,n) matrix.

The 0-padding is needed. The size of the padding, 
or the boundary condition as mentioned in the document, is determined by the stride size used by the sliding window.
The 2D Discrete Cross-Correlation formula used in the document implicitly assumes using stride of 1. 
Therefore the GEMM implementation is using stride of 1 for comparison. 


 
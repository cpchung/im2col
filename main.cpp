#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

typedef vector<int> vector_cp;
typedef vector<vector_cp> matrix_cp;

Eigen::MatrixXi vector2eigen(matrix_cp data) {
  Eigen::MatrixXi eMatrix(data.size(), data[0].size());
  for (int i = 0; i < data.size(); ++i)
    eMatrix.row(i) = Eigen::VectorXi::Map(&data[i][0], data[0].size());
  return eMatrix;
}

void fill_row(vector_cp &row) {
  std::generate(row.begin(), row.end(), []() { return rand() % 100; });
}

void fill_matrix(matrix_cp &mat) {
  std::for_each(mat.begin(), mat.end(), fill_row);
}

int cross_correlation(matrix_cp G_matrix, matrix_cp F_matrix, int x, int y) {

  int n = G_matrix.size();
  int k = F_matrix.size();
  int sum = 0;

  for (int i = 0; i < k; i++) {
    for (int j = 0; j < k; j++) {
      int xx = x + i - floor(k / 2);
      int yy = y + j - floor(k / 2);
      if (0 <= xx && xx < n && 0 <= yy && yy < n) {
        sum += F_matrix[i][j] * G_matrix[xx][yy];
      }
    }
  }
  return sum;
}

MatrixXi gemm(matrix_cp G_matrix, matrix_cp F_matrix) {
  const int S = 1;

  const int F = F_matrix.size();
  const int W = G_matrix.size();

  const int P = (F - 1) / 2;
  const int block_num = (W - F + 2 * P) / S + 1;

  const int length = W + 2 * P;

  matrix_cp padded_arr(length, vector_cp(length));

  //0-padding
  for (int i = 0; i < W; i++) {
    for (int j = 0; j < W; j++) {
      padded_arr[i + P][j + P] = G_matrix[i][j];
    }
  }

  Matrix<int, Dynamic, Dynamic> padded_input = vector2eigen(padded_arr);

  Eigen::MatrixXi flattened_block_matrix(F * F, 0);
  for (int i = 0; i < block_num; i++)
    for (int j = 0; j < block_num; j++) {
      //sliding window
      Matrix<int, Dynamic, Dynamic, RowMajor> padded_block = padded_input.block(S * i, S * j, F, F);
      //flatten sliding window into column vector
      Map<VectorXi> flattened_block(padded_block.data(), padded_block.size());
      flattened_block_matrix.conservativeResize(flattened_block_matrix.rows(), flattened_block_matrix.cols() + 1);
      //appending column vector into matrix
      flattened_block_matrix.col(flattened_block_matrix.cols() - 1) = flattened_block;

    }

  Matrix<int, Dynamic, Dynamic, RowMajor> filter = vector2eigen(F_matrix);
  Map<RowVectorXi> flattend_filter(filter.data(), filter.size());
//  matrix multiplication btw input matrix and filter
  Matrix<int, Dynamic, Dynamic> res(flattend_filter * flattened_block_matrix);
  Map<MatrixXi> res_reshaped(res.data(), block_num, block_num);
  return res_reshaped;
}

int main() {

  for (int n = 7; n <= 64; n++) {
    for (int k = 1; k <= 11 && k <= n; k = k + 2) {

      matrix_cp G_matrix(n, vector<int>(n));
      fill_matrix(G_matrix);

      matrix_cp F_matrix(k, vector<int>(k));
      fill_matrix(F_matrix);

      int res[n][n];
      Map<MatrixXi>(&res[0][0], n, n) = gemm(G_matrix, F_matrix);

      /*verification*/
      bool is_correct = true;
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          if (res[i][j] != cross_correlation(G_matrix, F_matrix, i, j))
            throw new runtime_error("incorrect");
          is_correct &= (res[i][j] == cross_correlation(G_matrix, F_matrix, i, j));

        }
      }

      printf("test case ( n = %d, k =%d ) is correct: %d\n", n, k, is_correct);
    }
  }

}
